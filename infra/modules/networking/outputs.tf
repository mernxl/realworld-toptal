output "vpc_id" {
  value       = module.vpc.vpc_id
  description = "ID of the created VPC"
}
