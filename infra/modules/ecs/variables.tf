variable "environment" {
  type        = string
  description = "Specify Environment, to be used in naming resources for an environment"
}

variable "app_name" {
  type        = string
  description = "A unique name to our application"
}

variable "app_image_ecr_url" {
  type        = string
  default     = null
  description = "Image URL for pulling image, if null will create ecr repo. Used for other environments"
}

variable "app_image_tag" {
  type        = string
  description = "Tag to be pulled from URL"
}

variable "allow_ecr_from_account_ids" {
  type        = list(string)
  default     = []
  description = "Only pass in base env, when a target env's account isn't same as ecr env account"
}

variable "vpc_id" {
  type        = string
  description = "VPC ID to launch the ecs in"
}

variable "db_url_secret_arn" {
  type        = string
  description = "ARN of the DB URL secrets to be used by app"
}
