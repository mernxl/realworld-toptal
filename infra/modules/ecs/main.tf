locals {
  app_port = 3000
}

resource "aws_ecs_cluster" "app" {
  name = "${var.app_name}-${var.environment}"

  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}

resource "aws_ecs_cluster_capacity_providers" "fargate" {
  cluster_name = aws_ecs_cluster.app.name

  capacity_providers = ["FARGATE", "FARGATE_SPOT"]

  default_capacity_provider_strategy {
    base              = 1
    weight            = 100
    capacity_provider = "FARGATE"
  }
}

module "app-alb" {
  source = "./alb"

  environment       = var.environment
  Tier              = "app"
  vpc_id            = var.vpc_id
  app_name          = var.app_name
  app_port          = local.app_port
  health_check_path = "/"
}

#tfsec:ignore:aws-ecr-repository-customer-key
#tfsec:ignore:aws-ecr-enforce-immutable-repository
resource "aws_ecr_repository" "app" {
  count = var.app_image_ecr_url == null ? 1 : 0

  name                 = "${var.app_name}-${var.environment}"
  image_tag_mutability = "MUTABLE" # allow so we can always set latest

  image_scanning_configuration {
    scan_on_push = true
  }

  tags = {
    Tier = "app"
  }
}

resource "aws_ecr_registry_policy" "app_ecr_policy" {
  count = length(var.allow_ecr_from_account_ids) == 0 ? 0 : 1 # only pass in base env, when a target env's account isn't same as ecr env account

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Sid    = "allowAccess",
        Effect = "Allow",
        Principal = {
          "AWS" : [
            for account_id in var.allow_ecr_from_account_ids : "arn:${data.aws_partition.current.partition}:iam::${account_id}:root"
          ]
        },
        Action = [
          "ecr:GetDownloadUrlForLayer",
          "ecr:BatchGetImage",
          "ecr:BatchCheckLayerAvailability",
        ],
        Resource = [
          "arn:${data.aws_partition.current.partition}:ecr:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:repository/${aws_ecr_repository.app[0].name}"
        ]
      }
    ]
  })
}

resource "random_string" "server-secret" {
  length  = 20
  special = false # don't add special characters
}

#tfsec:ignore:aws-ssm-secret-use-customer-key
resource "aws_secretsmanager_secret" "server-secret" {
  name = "terraform-managed-${var.app_name}-server-secret-${var.environment}"

  tags = {
    Tier = "app"
  }
}

resource "aws_secretsmanager_secret_version" "server-secret" {
  secret_id     = aws_secretsmanager_secret.server-secret.id
  secret_string = random_string.server-secret.result
}

module "app-ecs" {
  source = "./service"

  environment = var.environment
  Tier        = "app"

  cluster_id    = aws_ecs_cluster.app.id
  cluster_name  = aws_ecs_cluster.app.name
  vpc_id        = var.vpc_id
  app_name      = var.app_name
  app_port      = local.app_port
  image_ecr_url = var.app_image_ecr_url != null ? var.app_image_ecr_url : aws_ecr_repository.app[0].repository_url
  image_tag     = var.app_image_tag

  autoscaling_settings = {
    min_capacity         = 2
    max_capacity         = 4
    target_request_value = 100
  }
  alb_arn_suffix              = module.app-alb.lb_arn_suffix
  alb_target_group_arn        = module.app-alb.lb_target_group_arns[0]
  alb_target_group_arn_suffix = module.app-alb.lb_target_group_arn_suffixes[0]

  task_environment_variables = [
    { name = "HOST", value = "0.0.0.0" },
    { name = "PORT", value = local.app_port },
    { name = "SALT_ROUNDS", value = 10 }
  ]

  task_secret_environment_variables = [
    { name = "DATABASE_URL", valueFrom = var.db_url_secret_arn },
    { name = "SERVER_SECRET", valueFrom = aws_secretsmanager_secret.server-secret.arn }
  ]

  depends_on = [module.app-alb]
}
