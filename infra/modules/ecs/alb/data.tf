data "aws_vpc" "vpc" {
  id = var.vpc_id
}

data "aws_subnets" "tier" {
  filter {
    name   = "vpc-id"
    values = [var.vpc_id]
  }
  tags = {
    isPublic = "true"
  }
}
