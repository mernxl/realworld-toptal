module "security_group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.0"

  name        = "terraform-managed-${var.app_name}-alb-${var.environment}"
  description = "Security group for Public usage with ALB"
  vpc_id      = data.aws_vpc.vpc.id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["http-80-tcp", "all-icmp"]
  egress_rules        = ["all-all"]

  tags = {
    Tier = var.Tier
  }
}


module "alb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "~> 6.3"

  name = "${var.app_name}-alb-${var.environment}"

  load_balancer_type = "application"

  vpc_id          = var.vpc_id
  subnets         = data.aws_subnets.tier.ids
  security_groups = [module.security_group.security_group_id]

  # access_logs = {
  #   bucket = "my-alb-logs"
  # }

  target_groups = [
    {
      name             = "${var.app_name}-tg-${var.environment}"
      backend_protocol = "HTTP"
      backend_port     = var.app_port
      target_type      = "ip"

      health_check = {
        timeout  = 15 # our migrations
        interval = 30
        path     = var.health_check_path
      }
    }
  ]

  http_tcp_listeners = [
    {
      port               = 80
      protocol           = "HTTP"
      target_group_index = 0
    }
  ]

  tags = {
    Tier = var.Tier
  }

  target_group_tags = {
    Tier = var.Tier
  }

  http_tcp_listener_rules_tags = {
    Tier = var.Tier
  }

  http_tcp_listeners_tags = {
    Tier = var.Tier
  }
}
