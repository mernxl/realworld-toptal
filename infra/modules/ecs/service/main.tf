#tfsec:ignore:aws-cloudwatch-log-group-customer-key
resource "aws_cloudwatch_log_group" "task" {
  name = "/aws/ecs/service/${var.app_name}-${var.Tier}-${var.environment}"

  tags = {
    Tier = var.Tier
  }
}

resource "aws_security_group" "allow_app" {
  name        = "terraform-managed-${var.app_name}-${var.Tier}-service-${var.environment}"
  description = "Application Tier Traffic"
  vpc_id      = data.aws_vpc.vpc.id

  ingress {
    description = "Service access"
    from_port   = var.app_port
    to_port     = var.app_port
    protocol    = "tcp"
    cidr_blocks = [data.aws_vpc.vpc.cidr_block]
  }

  egress {
    description = "Allow all outbound traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    #tfsec:ignore:aws-vpc-no-public-egress-sgr
    cidr_blocks = ["0.0.0.0/0"] # required to allow ecr fetching
  }

  tags = {
    Name = "terraform-managed-${var.app_name}-${var.Tier}-service-${var.environment}"
    Tier = var.Tier
  }
}

resource "aws_ecs_task_definition" "task" {
  family = "${var.app_name}-${var.Tier}-${var.environment}"

  execution_role_arn = aws_iam_role.ExecutionRole.arn

  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]

  # Fargate cpu/mem must match available options: https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task-cpu-memory-error.html
  cpu    = var.fargate_cpu
  memory = var.fargate_mem

  container_definitions = jsonencode(
    [
      {
        name      = var.app_name
        image     = "${var.image_ecr_url}:${var.image_tag}"
        cpu       = var.container_cpu
        memory    = var.container_mem
        essential = true
        portMappings = [
          {
            containerPort = var.app_port
            hostPort      = var.app_port
          }
        ]
        environment = concat([{ name : "PORT", value : tostring(var.app_port) }], var.task_environment_variables)
        secrets     = var.task_secret_environment_variables == [] ? null : var.task_secret_environment_variables
        logConfiguration : {
          logDriver : "awslogs",
          options : {
            awslogs-group : aws_cloudwatch_log_group.task.name,
            awslogs-region : data.aws_region.current_region.name,
            awslogs-stream-prefix : var.app_name
          }
        }
      }
    ]
  )
}

resource "aws_iam_role" "ExecutionRole" {
  name = "terraform-managed-${var.app_name}-${var.Tier}-execution-role-${var.environment}"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ecs-tasks.amazonaws.com"
        }
      },
    ]
  })

  tags = {
    Name = "terraform-managed-${var.app_name}-${var.Tier}-execution-role-${var.environment}"
    Tier = var.Tier
  }
}

data "aws_iam_policy_document" "ecs_secrets_access" {
  count = length(var.task_secret_environment_variables)
  statement {
    sid       = "EcsSecretAccess"
    resources = [for secret in var.task_secret_environment_variables : secret.valueFrom]
    actions = [
      "secretsmanager:GetSecretValue",
    ]
  }
}
resource "aws_iam_role_policy" "ecs_secrets_access_role_policy" {
  count  = length(var.task_secret_environment_variables)
  name   = "terraform-managed-${var.app_name}-${var.Tier}-execution-role-secret-${var.environment}"
  role   = aws_iam_role.ExecutionRole.id
  policy = data.aws_iam_policy_document.ecs_secrets_access[count.index].json
}

# give basic ecs, ecr permissions
resource "aws_iam_role_policy_attachment" "ExecutionRole_to_ecsTaskExecutionRole" {
  role       = aws_iam_role.ExecutionRole.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

resource "aws_ecs_service" "this" {
  name            = "${var.app_name}-${var.Tier}-${var.environment}"
  cluster         = var.cluster_id
  task_definition = aws_ecs_task_definition.task.arn

  desired_count = 1

  launch_type      = "FARGATE"
  platform_version = "LATEST"

  network_configuration {
    assign_public_ip = false
    subnets          = data.aws_subnets.tier.ids
    security_groups  = [aws_security_group.allow_app.id]
  }

  load_balancer {
    target_group_arn = var.alb_target_group_arn
    container_name   = var.app_name
    container_port   = var.app_port
  }

  deployment_maximum_percent         = 100
  deployment_minimum_healthy_percent = 0

  # Ignored desired count changes live, permitting schedulers to update this value without terraform reverting
  lifecycle {
    ignore_changes = [desired_count]
  }

  tags = {
    Tier = var.Tier
  }
}

# Autoscaling Target
resource "aws_appautoscaling_target" "ecs_target" {
  max_capacity       = lookup(var.autoscaling_settings, "max_capacity", 1)
  min_capacity       = lookup(var.autoscaling_settings, "min_capacity", 1)
  resource_id        = "service/${var.cluster_name}/${aws_ecs_service.this.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

resource "aws_appautoscaling_policy" "ecs_policy_requests" {
  name               = "terraform-managed-${var.app_name}-${var.Tier}-scale-request"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.ecs_target.resource_id
  scalable_dimension = aws_appautoscaling_target.ecs_target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.ecs_target.service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ALBRequestCountPerTarget"
      resource_label         = "${var.alb_arn_suffix}/${var.alb_target_group_arn_suffix}"
    }

    target_value       = lookup(var.autoscaling_settings, "target_request_value", 100)
    scale_in_cooldown  = 300
    scale_out_cooldown = 300
  }
}
