output "db_host" {
  value       = module.this.db_instance_address
  description = "DB Host address"
}

output "db_port" {
  value       = module.this.db_instance_port
  description = "DB Port"
}

output "db_name" {
  value       = module.this.db_instance_name
  description = "DB name"
}

output "db_user" {
  value       = module.this.db_instance_username
  description = "DB User name"
}

output "db_pass" {
  value       = module.this.db_instance_password
  sensitive   = true
  description = "DB Password"
}

output "db_url_secret_arn" {
  value       = aws_secretsmanager_secret.db_url.arn
  sensitive   = true
  description = "Secrets arn, can be used by some other modules"
}
