variable "aws_region" {
  type        = string
  description = "Region to lunch Infrastructure in"
}

variable "vpc_cidr" {
  type        = string
  description = "VPC CIDR to use"
}

variable "environment" {
  type        = string
  description = "Specify Environment, to be used in naming resources for an environment"
}

variable "app_name" {
  type        = string
  description = "A unique name to our application"
}

variable "app_image_tag" {
  type        = string
  description = "Tag to deploy on app"
}

variable "app_image_ecr_url" {
  type        = string
  default     = null
  description = "Image URL for pulling image, if null will create ecr repo. Used for other environments"
}