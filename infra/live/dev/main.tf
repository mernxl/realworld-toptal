terraform {
  backend "s3" {
    bucket = "mernxl--terraform-states"
    key    = "realword-toptal-infra-dev"
    region = "eu-north-1"
  }
}

provider "aws" {
  region = var.aws_region

  default_tags {
    tags = {
      AppName     = var.app_name
      CreatedBy   = "Terraform"
      Environment = var.environment
    }
  }
}

locals {

}

module "networking" {
  source = "../../modules/networking"

  app_name    = var.app_name
  environment = var.environment
  aws_region  = var.aws_region
  vpc_cidr    = var.vpc_cidr
}

module "rds" {
  source = "../../modules/rds"

  app_name    = var.app_name
  environment = var.environment
  vpc_id      = module.networking.vpc_id
  Tier        = "db"

  random_password_length = 18

  db_engine_version = "13.4"
  depends_on        = [module.networking]
}

module "test-db" {
  source = "../../modules/rds"

  app_name    = var.app_name
  environment = "test"
  vpc_id      = module.networking.vpc_id
  Tier        = "db"

  random_password_length = 18

  db_engine_version = "13.4"
  depends_on        = [module.networking]
}

module "ecs" {
  source = "../../modules/ecs"

  app_name    = var.app_name
  environment = var.environment
  vpc_id      = module.networking.vpc_id

  app_image_tag = var.app_image_tag

  db_url_secret_arn = module.rds.db_url_secret_arn

  depends_on = [module.networking, module.rds]
}
