#!/bin/bash

file_or_dir=$1
commit_message=$2

if git diff --exit-code $file_or_dir; then
  echo "No new changes in $file_or_dir to commit..."
else 
  echo "Commiting changes as -> $commit_message"
  git add $file_or_dir
  git commit -m "$commit_message"
fi

