#!/bin/bash

# script allows us to use same image for running the applcation and migrations
set -e

cd prisma
echo "Running Migrations..."
# since prisma has a migration locking mechanism, this wouldn't bring problems
# if multiple containers are spun
yarn run prisma migrate deploy

echo "Starting Application..."
yarn run start

