# ![Rakkas RealWorld](logo.png)

> ### Rakkas codebase containing real world examples (CRUD, auth, advanced patterns, etc.) that adheres to the [RealWorld](https://github.com/gothinkster/realworld) spec and API.

### [Demo](https://realworld.rakkasjs.org/) &nbsp;&nbsp;&nbsp;&nbsp; [RealWorld](https://github.com/gothinkster/realworld)

> ### Scalable and Secure Continuous Delivery For a sample RealWorld App Implimentation [Rakkas Realworld](https://github.com/gothinkster/realworld).

---

## Application Requirements

| Check | Requirement                                                                                                                                | Implementation                                                            |
| :---: | :----------------------------------------------------------------------------------------------------------------------------------------- | :------------------------------------------------------------------------ |
|  ✅   | The architecture needs to be broken down into the relevant tiers (application, database, etc.).                                            | Broken into app and infra tiers                                           |
|  ✅   | The architecture should be completely provisioned via Chef/Puppet/Ansible/similar.                                                         | Terraform                                                                 |
|  ✅   | The deployment of new code and execution of tests should be completely automated. (Bonus points if you can catch performance regressions). | Gitlab CI                                                                 |
|  ✅   | The database and any mutable storage need to be backed up at least daily.                                                                  | AWS RDS automated Backups                                                 |
|  ✅   | All relevant logs for all tiers need to be easily accessible (having them on the hosts is not an option).                                  | Cloudwatch used for app, performance and DB logs                          |
|  ✅   | You should clone the repository and use it as the base for your system.                                                                    | Clone of [rakkas-realworld](https://github.com/rakkasjs/rakkas-realworld) |
|  ✅   | You should be able to deploy it on one larger Cloud provider: AWS / Google Cloud / Azure / DigitalOcean / RackSpace.                       | Deployed to AWS Cloud                                                     |
|  ✅   | The system should present relevant historical metrics to spot and debug bottlenecks.                                                       | App metrics are kept on Cloud watch                                       |

## Extra Features

- Ability to deploy to multiple environments (qa, prod) by making very few changes.
- Terraform Test, plan reports visible at MR
- Terraform Linting
- Terraform Static Security analysis
- Terraform Reports Presentation in MRs
- Other Unit Test Reports Artificats
- Build and deployment of Images of all Tiers to ECR
- GitLab [Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/) Integration

## App dependent implementation Notes

The app is based on a monorepo structure, uses Prisma at DB layer. Prisma needs to generate and deploy migrations (schema) to the database before application
can start working.

- Prisma Generates client on build container.
- Prisma Migrations are done at application start (Added in container entrypoint script). A Job has been commented out which deploys migrations from
  CD Pipeline, this would require making db instances public, so we prioritize security over ~10seconds of app start.

## Workflow

The Continuous delivery architecture is made around the **GitHub Flow** workflow.

- Feature branches are created base on tier (infra, app) dir, this will trigger the appropriate jobs. These jobs include basic tests.
- Upon merge, the changes are automatically deployed to `dev` environment.

## Trigger Pipeline

Use the following environment variables to trigger the pipeline for specific tiers and environments.

- **RUN_TIERS**: Will run all jobs will this tiers
- **RUN_ENVS**: Will run all jobs associated with this environments
- **BUILD_CI_IMAGE**: Will trigger a job to build the runner image, it is auto triggered if changes are made to root Dockerfile

## Required GitLab CI Environment Variables

The current implementation is based on two environments, dev and qa all in same account.

- **AWS_ACCESS_KEY_ID**: Access Key to use for deployments
- **AWS_SECRET_ACCESS_KEY**: Access Secret for use for deployments
- **AWS_REGION_DEV**: Dev region to use
- **AWS_REGION_QA**: QA region to use
- **GLO_ADMIN_GIT_NAME**: Git Admin Name, for committing
- **GLO_ADMIN_GIT_EMAIL**: Git Admin Email for committing
- **GLO_ADMIN_SSH_PRIVATE_KEY**: Git Admin SSH for use when committing
